from django.db import models
from datetime import datetime as dt

class Location(models.Model):
    place = models.CharField(max_length=100)
    lat = models.CharField(max_length=100)
    long = models.CharField(max_length=100)
    createdat = models.DateTimeField(default=dt.now, null=True, blank=True)

    def __str__(self):
        return self.place