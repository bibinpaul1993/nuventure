from datetime import datetime, timedelta

from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from .models import Location
from django import db
scheduler = BackgroundScheduler()
scheduler.add_jobstore(DjangoJobStore(), "default")

@register_job(scheduler, "interval", seconds=10, replace_existing=True)
def delete_ip():
    db.connections.close_all()
    dataset = Location.objects.all()
    if len(dataset) > 0:
        for record in dataset:
            # I assumed here that date_time is time when your object was created
            time_elapsed = datetime.now() - record.createdat.replace(tzinfo=None)
            if time_elapsed > timedelta(hours=24):
               record.delete()



try:
    scheduler.start()
except KeyboardInterrupt:
    scheduler.shutdown()
    print("Scheduler shut down successfully!")
