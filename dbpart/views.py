from django.shortcuts import render
from django.http import HttpResponse
from .models import Location


from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import *
from rest_framework import authentication
from rest_framework import generics, permissions, serializers, authentication
from .serializers import *
import requests
import datetime

from xml.etree import ElementTree

def index(request):
    return HttpResponse("Welcome")

def about(request):
    return HttpResponse("about")

class LocationReturn(generics.ListAPIView):
        serializer_class = LocationSerializer
        queryset = Location.objects.all()

        def check_date(self, place):
            """
           checking expiration date
            """
            print(place)

            if place.createdat.replace(tzinfo=None) <= datetime.datetime.now() - datetime.timedelta(minutes=5):
                place.delete()
                return False

        def get(self, request, *args, **kwargs):
            flag = False
            response = {'name' : ''}
            longitude = self.kwargs['lon']
            latitude = self.kwargs['lat']
            location = Location()
            dataset = Location.objects.filter(lat=latitude,long=longitude).all()
            print('data',dataset)
            if len(dataset) > 0:
                flag = self.check_date(dataset[0])
            if flag == False or len(dataset) == 0:
                url = 'https://nominatim.openstreetmap.org/reverse?'
                url = url + 'lat=' + latitude
                url = url + '&lon=' + longitude
                resp = requests.get(url)
                if resp.status_code == 200:
                    tree = ElementTree.fromstring(resp.content)
                    place = tree.find('result').text
                    location.place = place
                    location.lat = latitude
                    location.long = longitude
                    location.save()
                    response['name'] = place
                else:

                    response['name'] = str(dataset[0])
            return Response(response)
