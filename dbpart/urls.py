from django.urls import path
from . import views
from .tasks import scheduler

urlpatterns = [
    path('', views.index),
    path('about', views.about),
    path('loc/<str:lat>/<str:lon>', views.LocationReturn.as_view())

]

